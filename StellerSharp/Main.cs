﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace StellerSharp
{
    public partial class Main : Form
    {
        public static About about;
        public static FoodEntry foodEntry;

        public Main()
        {
            InitializeComponent();
            about = new About(this);
            foodEntry = new FoodEntry(this);
           // about.MdiParent = this; //produce an eror
        }

        MySqlConnection connect = new MySqlConnection("datasource=localhost; port=3306; username=root; password= ;database=sql_db;");
        int IsWhereSudah;

        private void button2_Click(object sender, EventArgs e)
        {
                try
                {
                    connect.Open();
                    IsWhereSudah = 0;
                   
                    string query = "select nama_makanan, harga_makanan, nama_restoran, alamat, jam_buka, jam_tutup, rating, halal from restoran ";
                query = tambah(query, this.boxLokasi.Text, "alamat");
                query = tambah(query, this.boxRating.Text, "rating");
                query = tambah(query, this.tbMakanan.Text, "nama_makanan");
                //query = tambah(query, this.checkBox1.CheckState, "halal");
                query = tambah(query, this.textBox2.Text, "harga_makanan");
                if (this.checkBox1.Checked)
                {
                    query = tambah(query, "1", "halal");
                }

                label4.Text = query;
                
                MySqlCommand read = new MySqlCommand(query, connect);
                    MySqlDataAdapter adapt = new MySqlDataAdapter();
                    adapt.SelectCommand = read;
                    DataTable data = new DataTable();
                    adapt.Fill(data);
                    dataGridView1.DataSource = data;
                    connect.Close();
                }

                catch (Exception ex)
                {
                    connect.Close();
                    MessageBox.Show(ex.Message);
                }
        }

        public string tambah (string query, string inputForWhere,string col)
        {
            if (inputForWhere!="") //ketika ada nilai input
            {
                if (IsWhereSudah == 0){ // ketika syarat belum ada
                    query += " where ";
                    IsWhereSudah++;
                }
                else {
                    query += " and ";
                }

                if (col=="harga_makanan")
                {
                    int harga = Convert.ToInt32(inputForWhere);
                    query += col + " <= " + harga + "";
                }
                else
                {
                    query += col + " = '" + inputForWhere + "'";
                }
                return query;
            }
            return query;
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tentangKamiToolStripMenuItem_Click(object sender, EventArgs e)
        {
           about.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tambahRestoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foodEntry.Show();
        }
    }
}
