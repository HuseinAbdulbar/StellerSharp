﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace StellerSharp
{
    public partial class FoodEntry : Form
    {
        private Main m;
        public FoodEntry(Main main)
        {
            InitializeComponent();
            m = main;
        }

        Boolean aktif = false;

        MySqlConnection connect = new MySqlConnection("datasource=localhost; port=3306; username=root; password= ;database=sql_db;");

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Hide();

        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            try
            {
                connect.Open();
                string a = "select * from restoran";
                MySqlCommand read = new MySqlCommand(a, connect);
                MySqlDataAdapter adapt = new MySqlDataAdapter();
                adapt.SelectCommand = read;
                DataTable data = new DataTable();
                adapt.Fill(data);
                dataGridView1.DataSource = data;
                connect.Close();
            }

            catch (Exception ex)
            {
                connect.Close();
                MessageBox.Show(ex.Message);
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                connect.Open();
                string a = "insert into restoran values('" + this.textBox1.Text + "','" + this.textBox2.Text +
                    "','" + this.textBox3.Text + "','" + this.textBox4.Text + "','" + this.textBox8.Text + "','"
                    + this.textBox6.Text + "','" + this.textBox7.Text + "','" + this.textBox5.Text + "','" + this.textBox9.Text + "')";
                MySqlCommand write = new MySqlCommand(a, connect);
                MySqlDataReader mdr;
                mdr = write.ExecuteReader();
                MessageBox.Show("saved");
                while (mdr.Read()) { }
                connect.Close();
            }

            catch (Exception ex)
            {
                connect.Close();
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                connect.Open();
                string a = "delete from restoran where id_restoran='" + this.textBox1.Text + "';";
                MySqlCommand MyCommand2 = new MySqlCommand(a, connect);
                MySqlCommand delete = new MySqlCommand(a, connect);
                MySqlDataReader mdr;
                mdr = delete.ExecuteReader();
                MessageBox.Show("Deleted");
                while (mdr.Read()) { }
                connect.Close();
          
            }
            catch (Exception ex)
            {
                connect.Close();
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                connect.Open();
                string a = "update restoran set nama_makanan='" + this.textBox2.Text + "', harga_makanan='" + this.textBox3.Text +
                    "', nama_restoran='" + this.textBox4.Text + "', alamat='" + this.textBox8.Text + "', jam_buka='" + 
                    this.textBox6.Text + "', jam_tutup='" + this.textBox7.Text + "', halal='" + this.textBox5.Text + 
                    "', rating='" + this.textBox9.Text + "' where id_restoran='" + this.textBox1.Text + "';";
                MySqlCommand update = new MySqlCommand(a, connect);
                MySqlDataReader mdr;
                mdr = update.ExecuteReader();
                MessageBox.Show("Updated");
                while (mdr.Read()) { }
                connect.Close();
            }
            catch (Exception ex)
            {
                connect.Close();
                MessageBox.Show(ex.Message);
            }
        }

     
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            aktif = true;
            
            string id = "";
            string nama = "";
            string harga = "";
            string resto = "";
            string alamat = "";
            string buka = "";
            string tutup = "";
            string halal = "";
            string rating = "";

            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                id = row.Cells[0].Value.ToString();
                nama = row.Cells[1].Value.ToString();
                harga = row.Cells[2].Value.ToString();
                resto = row.Cells[3].Value.ToString();
                alamat = row.Cells[4].Value.ToString();
                buka = row.Cells[5].Value.ToString();
                tutup = row.Cells[6].Value.ToString();
                halal = row.Cells[7].Value.ToString();
                rating = row.Cells[7].Value.ToString();

            }

            textBox1.Text = id ;
            textBox2.Text = nama;
            textBox3.Text = harga;
            textBox4.Text = resto;
            textBox5.Text = alamat;
            textBox6.Text = buka;
            textBox7.Text = tutup;
            textBox8.Text = halal;
            textBox9.Text = rating;

            aktif = false;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView1_MouseEnter(object sender, EventArgs e)
        {
          
        }
    }
}
